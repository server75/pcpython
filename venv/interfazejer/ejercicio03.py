#POSICIONAMIENTO
from tkinter import *
ventana=Tk()
ventana.title("Ventana de posicionamiento")
#POSICIONAMIENTOS CON GRID
#Posicionamiento del boton (.grid(row=0, column=1)
btnposi = Button(ventana, text="Posicionamiento diferente").grid(row=0, column=1)
#Posicionamiento del texto
lblposi1 = Label(ventana, text="Posicionamiento 1").grid(row=0, column=0)
lblposi2 = Label(ventana, text="Posicionamiento 2").grid(row=1, column=1)
lblposi3 = Label(ventana, text="Posicionamiento 3").grid(row=2, column=2)

ventana.mainloop()
