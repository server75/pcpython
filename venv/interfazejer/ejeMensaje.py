#Importar la libreria mensaje
from tkinter import messagebox
#mensaje de información - showinfo
messagebox.showinfo("Mensaje", "NAPAY PACHA")
#mensaje de advertencia - showwarning
messagebox.showwarning("Advertencia", "Estos es una advertencia")
#Mensaje de pregunta - askquestion
messagebox.askquestion("Pregunta 1", "Cualquier cosa")
#Mensaje de pregunta de cancelación - askokcancel
messagebox.askokcancel("Pregunta 2", "Cualquier cosa")
#Mensaje de pregunta que retorna un True o False - askyesno
messagebox.askyesno("Pregnuta 3","Cualquier cosa")
#Mnesaje que pregunta cancela un False y retorna un True - askretrycancel
messagebox.askretrycancel("Pregunta 1","Cualquier cosa")