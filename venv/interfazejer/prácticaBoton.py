from tkinter import *
def imprimir():
    print("Bienvenido a Python")
ventana= Tk()
ventana.title("Ventana de Inicio")
#Definir el color de las letras "fg" -  salir (.quit)
btnsalir = Button(ventana, text="Salir",fg="red", command=ventana.quit)
#Definir la dirección del boton (side=LEFT) - izquierda
btnsalir.pack(side=LEFT)
btnimprimir = Button(ventana, text="Imprimir",fg="blue", command=imprimir)
#Definir la dirección del boton (side=RIGHT) - derecha
btnimprimir.pack(side=RIGHT)
ventana.mainloop()