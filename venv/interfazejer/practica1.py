from tkinter import *
import time
from tkinter import messagebox
ventana = Tk()
def imprimir():
    print("Bienvenido a la Python")
def tiempo():
    ventana.iconify()
    time.sleep(3)
    ventana.deiconify()
ventana.title("Ventana de Práctica")
lblposi1 = Label(ventana, text="Saludo:", font=("arial", 18)).grid(row=0, column=0)
btnposi = Button(ventana, text="Hacer Click", command=imprimir, font=("arial", 20)).grid(row=0, column=1)
lblposi2 = Label(ventana, text="minimizar por 3 segundos:", font=("arial", 18)).grid(row=1, column=0)
btnposi2= Button(ventana, text="Click", command=tiempo, font=("arial", 20)).grid(row=1, column=1)
btnsalir = Button(ventana, text="Salir", command=ventana.quit, fg="red", font=("arial", 20)).grid(row=2, column=1)
ventana.mainloop()
