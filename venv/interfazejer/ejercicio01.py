#Importar la libreria para la interfaz
from tkinter import *
#Creación de la ventana
ventana = Tk()
#Colocar titulo a la ventana
ventana.title("VENTANA DE INICIO")
#Crear label
lab=Label(ventana, text="NAPAY PACHA")
lab.pack()
#Crear boton -  minimizar la ventana con el comando .iconify
bo=Button(ventana, text="Minimizar", font=("amigos", 20), command=ventana.iconify)
bo.pack()
#Definir el método
ventana.mainloop()