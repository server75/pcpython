from tkinter import *
def operacion():
    numero = num.get()
    if opcion.get() == 1:
        total = numero * 10
    elif opcion.get() == 2:
        total = numero * 20
    elif opcion.get() == 3:
        total = numero * 30
    elif opcion.get() == 4:
        total = numero * 40
    elif opcion.get() == 5:
        total = numero * 50
    else:
        total = numero * numero
    print("EL total de la operación es: " + str(total))
ventana = Tk()
opcion = IntVar()
num = IntVar()
ventana.title("RadioBoton en Tkinker")
ventana.geometry("400x300")
lblnu = Label(ventana, text="Escribir un número:").place(x=20, y=20)
txtnum = Entry(ventana, textvariable=num).place(x=130, y=20)
lblop = Label(ventana, text="Seleccionar").place(x=20, y=50)
#Syntasis de Radiobutton
op01 = Radiobutton(ventana, text="x10", value=1, variable=opcion).place(x=20,y=80)
op02 = Radiobutton(ventana, text="x20", value=2, variable=opcion).place(x=70,y=80)
op03 = Radiobutton(ventana, text="x30", value=3, variable=opcion).place(x=120,y=80)
op04 = Radiobutton(ventana, text="x40", value=4, variable=opcion).place(x=20,y=110)
op05 = Radiobutton(ventana, text="x50", value=5, variable=opcion).place(x=70,y=110)
cuadrado = Radiobutton(ventana, text="Cuadrado", value=6, variable=opcion).place(x=120,y=110)
btncalcular = Button(ventana, text="CALCULAR", command=operacion).place(x=20,y=140)
ventana.mainloop()