from tkinter import *
def saludar():
    print("hola: "+nombre.get()+" "+apellidoP.get()+" "+apellidoM.get())

ventana = Tk()
ventana.title("Registro")
ventana.geometry("300x150")
nombre=StringVar()
apellidoP=StringVar()
apellidoM=StringVar()
nombre.set("Escribir el nombre")
lblnom = Label(ventana, text="Escribir el nombre: ").place(x=10, y=10)
txtnom = Entry(ventana, textvariable=nombre).place(x=170, y=10)
lblapeP = Label(ventana, text="Escribir apellido Paterno: ").place(x=10,y=40)
txtapeP = Entry(ventana, textvariable=apellidoP).place(x=170,y=40)
lblapeM = Label(ventana, text="Escribir el apellido Materno: ").place(x=10,y=70)
txtapeM = Entry(ventana, textvariable=apellidoM).place(x=170,y=70)
btnsa = Button(ventana, text="Saludo Personalizado",bg="black", fg="red", command=saludar, font=("arial", 15)).place(x=50, y=100)
ventana.mainloop()
