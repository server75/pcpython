from tkinter import *
#Importar el libreria de mensaje
from tkinter import messagebox

def obtener():
    #print(valor.get())
    #Creación de la ventana de mensaje
    messagebox.showinfo("Mensaje","Tu Seleccionaste: "+valor.get())

ventana= Tk()
valor = StringVar()
ventana.title("Ventana de Spinbox")
ventana.geometry("400x300")
lblti1 = Label(ventana, text="Ejemplo 1 de Spinbox").place(x=20,y=20)
#Definiendo la (a) estructura de Spinbox por nombres seleccionados
combo = Spinbox(ventana, values=("Uno","Dos","Tres","Cuatro","Cinco")).place(x=20,y=50)
lblti = Label(ventana, text="Ejemplo 2 de Spinbox").place(x=20,y=80)
#Definiendo la (b) estructura de Spinbox establecidos con números establecidos
#"from_= (desde)" "to=(hasta)"
combo2 = Spinbox(ventana, from_=1, to=10, textvariable=valor).place(x=20,y=110)
btnobtener = Button(ventana, text="Obtener valor Spinbox", command=obtener).place(x=80, y=140)
ventana.mainloop()